package tp.gli;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.stream.Collectors;

public class ModelImpl extends Observable implements CamembertModel {


    private List<Entry<String, Integer>> entries;

    private String title;

    private String description;
    
    private String unit;

    public ModelImpl(){
        this.entries = new ArrayList<>();
    }
    
    @Override
    public int size() {
        return entries.size();
    }

    @Override
    public double getValues(int i) {
        return entries.get(i).getValue();
    }

    @Override
    public double total() {
        int total = entries.stream().map(e -> e.getValue()).collect(Collectors.summingInt(Integer::intValue));
        return total;
    }

    @Override
    public String getTitle(int i) {
        return entries.get(i).getKey();
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public String getDescription(int i) {
        return description;
    }

    @Override
    public String getTitle() {
        return title;
    }

    /**
     * @return List<Entry<String, Integer>> return the entries
     */
    public List<Entry<String, Integer>> getEntries() {
        return entries;
    }

    /**
     * @param entry entry to add
     */
    public void addEntry(Entry<String, Integer> entry) {
        this.notifyObservers();
        this.entries.add(entry);
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.notifyObservers();
        this.title = title;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.notifyObservers();
        this.description = description;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.notifyObservers();
        this.unit = unit;
    }

}