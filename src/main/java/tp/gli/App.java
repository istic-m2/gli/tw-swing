package tp.gli;

import java.awt.GridLayout;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.JFrame;

/**
 * Hello world!
 */
public final class App {
    public static void main(String[] a) {
        // TODO: complete this method
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setBounds(30, 30, 400, 400);
        window.setSize(600, 600);

        // Create an instance of the model
        ModelImpl model = new ModelImpl();

        // Maybe put some data in the model

        model.setTitle("title");
        model.setDescription("description");
        model.setUnit("unit");
        model.addEntry(new SimpleEntry<String, Integer>("one", 250));
        model.addEntry(new SimpleEntry<String, Integer>("two", 250));
        model.addEntry(new SimpleEntry<String, Integer>("three", 250));
        model.addEntry(new SimpleEntry<String, Integer>("four", 250));

        int oldFirst = 0;
        int oldLast = 0;

        CamembertView view = new CamembertView(model);
        
        // Create the controller and link the controller to the model...
        Controller controller = new ControllerImpl(model, view);
        view.setController(controller);

        // display layout
        GridLayout layout = new GridLayout(1, 2);

        window.getContentPane().add(controller.getView());

        window.setLayout(layout);
        // window.pack();
        window.setVisible(true);
        window.addMouseListener(view);
        // window.pack();
    }
}
