package tp.gli;

import java.util.Observer;

public interface CamembertModel {

	void addObserver(Observer observer);

	int size();

	double getValues(int i);

	double total();

	String getTitle(int i);

	String getUnit();

	String getDescription(int i);

	String getTitle();
    

}