package tp.gli;

import java.awt.Component;

public class ControllerImpl implements Controller {

    private boolean selected;
    private int selectedPie;
    private CamembertModel model;
    private CamembertView view;

    public ControllerImpl(CamembertModel m, CamembertView v) {
        this.model = m;
        this.view = v;
        this.selected = false;
        this.selectedPie = 0;
    }

    @Override
    public void deSelect() {
        this.selected = false;

    }

    @Override
    public void nextPie() {
        selectedPie = (selectedPie + 1) % model.size();
    }

    @Override
    public void previousPie() {
        selectedPie = (model.size() + selectedPie - 1) % model.size();

    }

    /**
     * @return boolean return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * @return int return the selectedPie
     */
    public int getSelectedPie() {
        return selectedPie;
    }

    /**
     * @param selectedPie the selectedPie to set
     */
    public void setSelectedPie(int selectedPie) {
        this.selectedPie = selectedPie;
        this.selected = true;
    }

    /**
     * @return CamembertModel return the model
     */
    public CamembertModel getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(CamembertModel model) {
        this.model = model;
    }

    /**
     * @return CamembertView return the view
     */
    public CamembertView getView() {
        return view;
    }

    /**
     * @param view the view to set
     */
    public void setView(CamembertView view) {
        this.view = view;
    }

    @Override
    public void selectPie(int i) {
        this.selectedPie = i;
        this.selected = true;
    }

}